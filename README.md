# CAISAR

CAISAR (Characterizing AI Safety And Robustness) is a platform
under active development at CEA LIST, aiming to provide a
wide range of features to characterize the safety and robustness of
artificial intelligence based software.

## Getting CAISAR

The latest release of CAISAR is available as an [opam](https://opam.ocaml.org/)
package or a [Docker](https://www.docker.com/) image.

The development version of CAISAR is available only by compiling the source code.

### opam package

**Please note:** CAISAR requires the OCaml package manager [opam](https://opam.ocaml.org/),
v2.1 or higher, which is typically avaible in all major GNU/Linux distributions.

To install CAISAR via [opam](https://opam.ocaml.org/), do the following:
```
$ opam install caisar
```

### Docker image

A ready-to-use [Docker](https://www.docker.com/) image of CAISAR is available on
[Docker Hub](https://hub.docker.com). To retrieve such an image, do the
following:
```
$ docker pull laiser/caisar:pub
```

Alternatively, a [Docker](https://www.docker.com/) image for CAISAR can be
created locally by proceeding as follows:
```
$ git clone https://git.frama-c.com/pub/caisar
$ cd caisar
$ make docker
```

To run the CAISAR [Docker](https://www.docker.com/) image, do the following:
```
$ docker run -it laiser/caisar:pub sh
```

### From source code

**Please note:** CAISAR requires the OCaml package manager [opam](https://opam.ocaml.org/),
v2.1 or higher, which is typically avaible in all major GNU/Linux distributions.

To build and install CAISAR, do the following:

```
$ git clone https://git.frama-c.com/pub/caisar
$ cd caisar
$ opam switch create --yes --no-install . 4.13.1
$ opam install . --deps-only --with-test --yes
$ make
$ make install
```

To run the tests:
```
$ make test
```

## Usage

To start using CAISAR, please run the command:
```
$ caisar --help
```

### Property verification

CAISAR can be used to verify properties on neural networks and support-vector
machines (SVM).

The prototype command is:
```
$ caisar verify --prover=PROVER FILE
```

`FILE` defines the property to verify, and it must be
written in the [WhyML](https://why3.lri.fr/doc-0.80/manual004.html) language.
Examples of [WhyML](https://why3.lri.fr/doc-0.80/manual004.html) files (`.mlw`)
can be found in the [tests](https://git.frama-c.com/pub/caisar/-/tree/master/tests)
folder.

### External provers detection

CAISAR relies on external provers to work. These must be installed first,
then CAISAR must be instructed to point to their location. To do so, the
path to the prover executables should appear in the environment variable
`PATH`.

Run the following command to confirm that CAISAR detects the installed provers:
```
$ caisar config --detect
```

The following are the provers for which a support is provided in CAISAR:

* PyRAT (to be released)
* [Marabou](https://github.com/NeuralNetworkVerification/Marabou)
* [SAVer](https://github.com/abstract-machine-learning/saver)

Under active development is the support for the [SMT-LIB](https://smtlib.cs.uiowa.edu/) which
is used by many satisfiability modulo theories solvers (e.g. [Alt-Ergo](https://alt-ergo.ocamlpro.com/),
[Z3](https://github.com/Z3Prover/z3/wiki), [Colibri](https://colibri.frama-c.com/), etc.).


## Advanced usage

### How to add a solver

Make sure the solver is installed in your system. Typically, the path to its
executable should appear in the environment variable `PATH`. Then,

1. **Create a `solver.drv` in [config/drivers/](https://git.frama-c.com/pub/caisar/-/tree/master/config/drivers).**
A driver is a series of [WhyML](https://why3.lri.fr/doc-0.80/manual004.html) modules
describing the theories the solver is able to understand as provided by [Why3](https://why3.lri.fr/).
Directives for letting [Why3](https://why3.lri.fr/) interpret the solver outputs should also be provided here.

2. **Add a new record in [config/caisar-detection-data.conf](https://git.frama-c.com/pub/caisar/-/blob/master/config/caisar-detection-data.conf).** The name of the
solver executable should be provided , as well as a command-line template that
[Why3](https://why3.lri.fr/) will use for executing the solver. Such a template may specify several
[Why3](https://why3.lri.fr/) built-in identifiers:

    * `%e` stands for the executable
    * `%f` stands for a file to pass to the executable

Other custom identifiers have been added: `%{nnet-onnx}` and `%{svm}`. These
identifiers are used for providing the solver with potential `{nnet, onnx}` and
`svm` model filenames, respectively.

3. **Write a [Why3](https://why3.lri.fr/) printer.** The solver should be recognized by CAISAR by
now. However, a printer for the solver may be needed for transforming [Why3](https://why3.lri.fr/)
specifications into something the solver can understand. Printers should be
placed in [src/printers/](https://git.frama-c.com/pub/caisar/-/tree/master/src/printers).
