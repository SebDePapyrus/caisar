#!/bin/sh -e


case $1 in
    --version)
        echo "PyRAT 1.1"
        ;;
    *)
        echo "PWD: $(pwd)"
        echo "NN: $2"
        test -e $2 || (echo "Cannot find the NN file" && exit 1)
        echo "Goal:"
        cat $4
        echo "Result = Unknown"
esac
