Test autodetect
  $ cat - > bin/alt-ergo << EOF
  > #!/bin/sh
  > echo "2.4.0"
  > EOF

  $ chmod u+x bin/alt-ergo bin/pyrat.py bin/Marabou bin/saver

  $ bin/alt-ergo
  2.4.0

  $ bin/pyrat.py --version
  PyRAT 1.1

  $ bin/Marabou --version
  1.0.+

  $ bin/saver --version
  v1.0

  $ PATH=$(pwd)/bin:$PATH

  $ caisar config -d
  [caisar] Alt-Ergo 2.4.0
           Marabou 1.0.+
           PyRAT 1.1
           SAVer v1.0
