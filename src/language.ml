(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

(* Support for several model formats: *)
(* - NNet and ONNX for neural networks *)
(* - OVO for for SVM *)

type nnshape = {
  nb_inputs : int;
  nb_outputs : int;
  ty_data : Ty.ty;
  filename : string;
}

type svmshape = { nb_inputs : int; nb_classes : int; filename : string }

let loaded_nets = Term.Hls.create 10
let loaded_svms = Term.Hls.create 10
let lookup_loaded_nets = Term.Hls.find_opt loaded_nets
let lookup_loaded_svms = Term.Hls.find_opt loaded_svms

let register_nn_as_tuple nb_inputs nb_outputs filename env =
  let net = Pmodule.read_module env [ "caisar" ] "NN" in
  let ioshape_input_type =
    Ty.ty_app Theory.(ns_find_ts net.mod_theory.th_export [ "input_type" ]) []
  in
  let id_as_tuple = Ident.id_fresh "NNasTuple" in
  let th_uc = Pmodule.create_module env id_as_tuple in
  let th_uc = Pmodule.use_export th_uc net in
  let ls_net_apply =
    let f _ = ioshape_input_type in
    Term.create_fsymbol
      (Ident.id_fresh "net_apply")
      (List.init nb_inputs ~f)
      (Ty.ty_tuple (List.init nb_outputs ~f))
  in
  Term.Hls.add loaded_nets ls_net_apply
    { filename; nb_inputs; nb_outputs; ty_data = ioshape_input_type };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_net_apply))
  in
  Wstdlib.Mstr.singleton "NNasTuple" (Pmodule.close_module th_uc)

let register_svm_as_array nb_inputs nb_classes filename env =
  let svm = Pmodule.read_module env [ "caisar" ] "SVM" in
  let svm_type =
    Ty.ty_app Theory.(ns_find_ts svm.mod_theory.th_export [ "svm" ]) []
  in
  let id_as_array = Ident.id_fresh "SVMasArray" in
  let th_uc = Pmodule.create_module env id_as_array in
  let th_uc = Pmodule.use_export th_uc svm in
  let ls_svm_apply =
    Term.create_fsymbol (Ident.id_fresh "svm_apply") [] svm_type
  in
  Term.Hls.add loaded_svms ls_svm_apply { filename; nb_inputs; nb_classes };
  let th_uc =
    Pmodule.add_pdecl ~vc:false th_uc
      (Pdecl.create_pure_decl (Decl.create_param_decl ls_svm_apply))
  in
  Wstdlib.Mstr.singleton "SVMasArray" (Pmodule.close_module th_uc)

let nnet_parser env _ filename _ =
  let model = Nnet.parse filename in
  match model with
  | Error s -> Loc.errorm "%s" s
  | Ok model -> register_nn_as_tuple model.n_inputs model.n_outputs filename env

let onnx_parser env _ filename _ =
  let model = Onnx.parse filename in
  match model with
  | Error s -> Loc.errorm "%s" s
  | Ok model -> register_nn_as_tuple model.n_inputs model.n_outputs filename env

let ovo_parser env _ filename _ =
  let model = Ovo.parse filename in
  match model with
  | Error s -> Loc.errorm "%s" s
  | Ok model ->
    register_svm_as_array model.n_inputs model.n_outputs filename env

let register_nnet_support () =
  Env.register_format ~desc:"NNet format (ReLU only)" Pmodule.mlw_language
    "NNet" [ "nnet" ] nnet_parser

let register_onnx_support () =
  Env.register_format ~desc:"ONNX format" Pmodule.mlw_language "ONNX" [ "onnx" ]
    onnx_parser

let register_ovo_support () =
  Env.register_format ~desc:"OVO format" Pmodule.mlw_language "OVO" [ "ovo" ]
    ovo_parser
