(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base
module Filename = Caml.Filename

module File = struct
  type t = Stdin | File of string

  let of_string s =
    if String.equal s "-"
    then `Ok Stdin
    else if Caml.Sys.file_exists s
    then `Ok (File s)
    else `Error (Fmt.str "no '%s' file or directory" s)

  let pretty fmt = function
    | Stdin -> Fmt.string fmt "-"
    | File f -> Fmt.string fmt f
end

let () =
  Language.register_nnet_support ();
  Language.register_onnx_support ();
  Language.register_ovo_support ()

let create_env loadpath =
  let config = Autodetect.autodetection ~debug:true () in
  let config =
    let main = Whyconf.get_main config in
    let dft_memlimit = 4000 (* 4 GB *) in
    let main =
      Whyconf.(
        set_limits main (timelimit main) dft_memlimit (running_provers_max main))
    in
    Whyconf.set_main config main
  in
  let stdlib = Dirs.Sites.stdlib in
  ( Env.create_env
      (loadpath @ stdlib @ Whyconf.loadpath (Whyconf.get_main config)),
    config )

let nnet_or_onnx = Re__Core.(compile (str "%{nnet-onnx}"))
let svm = Re__Core.(compile (str "%{svm}"))
let dataset = Re__Core.(compile (str "%{dataset}"))
let epsilon = Re__Core.(compile (str "%{epsilon}"))
let abstraction = Re__Core.(compile (str "%{abstraction}"))
let distance = Re__Core.(compile (str "%{distance}"))

let combine_prover_answers answers =
  List.fold_left answers ~init:Call_provers.Valid ~f:(fun acc r ->
    match (acc, r) with
    | Call_provers.Valid, r | r, Call_provers.Valid -> r
    | _ -> acc)

let answer_saver limit config task env prover dataset_csv =
  let handle_task_saver task env dataset_csv command =
    let dataset_filename =
      match dataset_csv with
      | None -> failwith "Error, no dataset provided for SAVer."
      | Some s -> s
    in
    let goal = Task.task_goal_fmla task in
    let eps, svm_filename =
      match goal.t_node with
      | Tquant (Tforall, b) -> (
        let _, _, pred = Term.t_open_quant b in
        let svm_t = Pmodule.read_module env [ "caisar" ] "SVM" in
        let robust_to_predicate =
          let open Theory in
          ns_find_ls svm_t.mod_theory.th_export [ "robust_to" ]
        in
        match pred.t_node with
        | Term.Tapp
            ( ls,
              [
                { t_node = Tapp (svm_app_sym, _); _ };
                _;
                { t_node = Tconst e; _ };
              ] ) ->
          if Term.ls_equal ls robust_to_predicate
          then
            let eps = Fmt.str "%a" Constant.print_def e in
            let svm_filename =
              match Language.lookup_loaded_svms svm_app_sym with
              | Some t -> t.filename
              | None -> failwith "SVM file not found in environment."
            in
            (eps, svm_filename)
          else failwith "Wrong predicate found."
        (* no other predicate than robust_to is supported *)
        | _ -> failwith "Unsupported by SAVer.")
      | _ -> failwith "Unsupported by SAVer."
    in
    let svm_file = Filename.concat (Caml.Sys.getcwd ()) svm_filename in
    let dataset_file = Filename.concat (Caml.Sys.getcwd ()) dataset_filename in
    let command = Re__Core.replace_string svm ~by:svm_file command in
    let command = Re__Core.replace_string dataset ~by:dataset_file command in
    let command = Re__Core.replace_string epsilon ~by:eps command in
    let command = Re__Core.replace_string distance ~by:"l_inf" command in
    let command = Re__Core.replace_string abstraction ~by:"hybrid" command in
    command
  in
  let command = Whyconf.get_complete_command ~with_steps:false prover in
  let command = handle_task_saver task env dataset_csv command in
  let res_parser =
    {
      Call_provers.prp_regexps =
        [ ("NeverMatch", Call_provers.Failure "Should not happen in CAISAR") ];
      prp_timeregexps = [];
      prp_stepregexps = [];
      prp_exitcodes = [];
      prp_model_parser = Model_parser.lookup_model_parser "no_model";
    }
  in
  let prover_call =
    Call_provers.call_on_buffer ~libdir:(Whyconf.libdir config)
      ~datadir:(Whyconf.datadir config) ~command ~limit ~res_parser
      ~filename:" " ~get_counterexmp:false ~gen_new_file:false
      ~printing_info:Printer.default_printing_info (Buffer.create 10)
  in
  let prover_result = Call_provers.wait_on_call prover_call in
  let answer =
    match prover_result.pr_answer with
    | Call_provers.HighFailure -> (
      let pr_output = prover_result.pr_output in
      let matcher =
        Re__Pcre.regexp
          "\\[SUMMARY\\]\\s*(\\d+)\\s*[0-9.]+\\s*[0-9.]+\\s*\\d+\\s*(\\d+)\\s*\\d"
      in
      match Re__Core.exec_opt matcher pr_output with
      | Some g ->
        if Int.of_string (Re__Core.Group.get g 1)
           = Int.of_string (Re__Core.Group.get g 2)
        then Call_provers.Valid
        else Call_provers.Invalid
      | None -> Call_provers.HighFailure
      (* Any other answer than HighFailure should never happen as we do not
         define anything in SAVer's driver. *))
    | _ -> assert false
  in
  answer

let answer_generic limit config task driver (prover : Whyconf.config_prover) =
  let task_prepared = Driver.prepare_task driver task in
  let tasks =
    (* We make [tasks] as a list (ie, conjunction) of disjunctions. *)
    if String.equal prover.prover.prover_name "Marabou"
    then Trans.apply Split_goal.split_goal_full task_prepared
    else [ task_prepared ]
  in
  let command = Whyconf.get_complete_command ~with_steps:false prover in
  let nn_file =
    match Task.on_meta_excl Native_nn_prover.meta_nn_filename task_prepared with
    | Some [ MAstr nn_file ] -> nn_file
    | Some _ -> assert false (* By construction of the meta. *)
    | None -> invalid_arg (Fmt.str "No neural network model found in task")
  in
  let nn_file = Unix.realpath nn_file in
  let command = Re__Core.replace_string nnet_or_onnx ~by:nn_file command in
  let call_prover_on_task task_prepared =
    let prover_call =
      Driver.prove_task_prepared ~libdir:(Whyconf.libdir config)
        ~datadir:(Whyconf.datadir config) ~command ~limit driver task_prepared
    in
    let prover_result = Call_provers.wait_on_call prover_call in
    prover_result.pr_answer
  in
  let answers = List.map tasks ~f:call_prover_on_task in
  let answer = combine_prover_answers answers in
  answer

let call_prover ~limit config (prover : Whyconf.config_prover) driver env
  dataset_csv task =
  let prover_answer =
    if String.equal prover.prover.prover_name "SAVer"
    then answer_saver limit config task env prover dataset_csv
    else answer_generic limit config task driver prover
  in
  Fmt.pr "Goal %a: %a@." Pretty.print_pr (Task.task_goal task)
    Call_provers.print_prover_answer prover_answer

let verify ?(debug = false) format loadpath ?memlimit ?timeout prover
  ?dataset_csv file =
  if debug then Debug.set_flag (Debug.lookup_flag "call_prover");
  let env, config = create_env loadpath in
  let steplimit = None in
  let steps = match steplimit with Some 0 -> None | _ -> steplimit in
  let limit =
    let memlimit =
      Option.value memlimit ~default:Whyconf.(memlimit (get_main config))
    in
    let def = Call_provers.empty_limit in
    {
      Call_provers.limit_time = Opt.get_def def.limit_time timeout;
      Call_provers.limit_steps = Opt.get_def def.limit_time steps;
      Call_provers.limit_mem = memlimit;
    }
  in
  let _, mstr_theory =
    match file with
    | File.Stdin ->
      ("stdin", Env.(read_channel ?format base_language env "stdin" Caml.stdin))
    | File file ->
      let mlw_files, _ = Env.(read_file ?format base_language env file) in
      (file, mlw_files)
  in
  Wstdlib.Mstr.iter
    (fun _ theory ->
      let tasks = Task.split_theory theory None None in
      let prover =
        let prover = Prover.to_string prover in
        Whyconf.(filter_one_prover config (mk_filter_prover prover))
      in
      let driver =
        match String.chop_prefix ~prefix:"caisar_drivers/" prover.driver with
        | None -> Whyconf.(load_driver (get_main config) env prover)
        | Some file ->
          let file =
            Filename.concat
              (Filename.concat (List.hd_exn Dirs.Sites.config) "drivers")
              file
          in
          Driver.load_driver_absolute env file prover.extra_drivers
      in
      List.iter
        ~f:
          (call_prover ~limit (Whyconf.get_main config) prover driver env
             dataset_csv)
        tasks)
    mstr_theory
