(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3

type nnshape = {
  nb_inputs : int;
  nb_outputs : int;
  ty_data : Ty.ty;
  filename : string;
}

type svmshape = { nb_inputs : int; nb_classes : int; filename : string }

val lookup_loaded_nets : Term.lsymbol -> nnshape option
(** @return the filename of a nnet Why3 representation. *)

val lookup_loaded_svms : Why3.Term.lsymbol -> svmshape option
(** @return the svmshape of a svm Why3 representation. *)

val register_nnet_support : unit -> unit
(** Register NNet parser. *)

val register_onnx_support : unit -> unit
(** Register ONNX parser. *)

val register_ovo_support : unit -> unit
(** Register OVO parser. *)
