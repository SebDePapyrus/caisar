(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Base
open Cmdliner

let caisar = "caisar"

let () =
  Native_nn_prover.init ();
  Vars_on_lhs.init ()

let () =
  Pyrat.init ();
  Marabou.init ()

(* -- Logs. *)

let pp_header =
  let x =
    match Array.length Caml.Sys.argv with
    | 0 -> Caml.(Filename.basename Sys.executable_name)
    | _ -> Caml.(Filename.basename Sys.argv.(0))
  in
  let pp_h ppf style h = Fmt.pf ppf "[%s][%a] " x Fmt.(styled style string) h in
  fun ppf (l, h) ->
    let open Logs_fmt in
    match l with
    | Logs.App -> Fmt.pf ppf "[%a] " Fmt.(styled app_style string) x
    | Logs.Error ->
      pp_h ppf err_style (match h with None -> "ERROR" | Some h -> h)
    | Logs.Warning ->
      pp_h ppf warn_style (match h with None -> "WARNING" | Some h -> h)
    | Logs.Info ->
      pp_h ppf info_style (match h with None -> "INFO" | Some h -> h)
    | Logs.Debug ->
      pp_h ppf debug_style (match h with None -> "DEBUG" | Some h -> h)

let setup_logs =
  let setup_log level =
    Fmt_tty.setup_std_outputs ~style_renderer:`Ansi_tty ();
    Logs.set_level level;
    Logs.set_reporter (Logs_fmt.reporter ~pp_header ())
  in
  Term.(const setup_log $ Logs_cli.level ())

let log_level_is_debug () =
  match Logs.level () with Some Debug -> true | _ -> false

(* -- Commands. *)

let config detect () =
  if detect
  then (
    Logs.debug (fun m -> m "Automatic detection.");
    let config =
      let debug = log_level_is_debug () in
      Autodetect.autodetection ~debug ()
    in
    let open Why3 in
    let provers = Whyconf.get_provers config in
    if not (Whyconf.Mprover.is_empty provers)
    then
      Logs.app (fun m ->
        m "@[<v>%a@]"
          (Pp.print_iter2 Whyconf.Mprover.iter Pp.newline Pp.nothing
             Whyconf.print_prover Pp.nothing)
          provers))

let verify format loadpath memlimit timeout prover dataset_csv files =
  let debug = log_level_is_debug () in
  List.iter
    ~f:
      (Verification.verify ~debug format loadpath ?memlimit ?timeout prover
         ?dataset_csv)
    files

let exec_cmd cmdname cmd =
  Logs.debug (fun m -> m "Command `%s'." cmdname);
  cmd ()

(* -- Command line. *)

let config_cmd =
  let cmdname = "config" in
  let info =
    let doc = Fmt.str "%s configuration." caisar in
    let exits = Cmd.Exit.defaults in
    let man =
      [
        `S Manpage.s_description;
        `P (Fmt.str "Handle the configuration of %s." caisar);
      ]
    in
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits ~doc ~man
  in
  let term =
    let detect =
      let doc = "Detect solvers in \\$PATH." in
      Arg.(value & flag & info [ "d"; "detect" ] ~doc)
    in
    Term.(
      ret
        (const (fun detect _ ->
           if not detect
           then `Help (`Pager, Some "config")
           else
             (* TODO: Do not only check for [detect], and enable it by default,
                as soon as other options are available. *)
             `Ok (exec_cmd cmdname (fun () -> config true ())))
        $ detect $ setup_logs))
  in
  Cmd.v info term

let verify_cmd =
  let cmdname = "verify" in
  let doc = "Property verification using external provers." in
  let info =
    Cmd.info cmdname ~sdocs:Manpage.s_common_options ~exits:Cmd.Exit.defaults
      ~doc
      ~man:[ `S Manpage.s_description; `P doc ]
  in
  let term =
    let files =
      let doc = "Files to verify." in
      let file_or_stdin = Verification.File.(of_string, pretty) in
      Arg.(value & pos_all file_or_stdin [] & info [] ~doc)
    in
    let format =
      let doc = "File format." in
      Arg.(value & opt (some string) None & info [ "format" ] ~doc)
    in
    let loadpath =
      let doc = "Additional loadpath." in
      Arg.(value & opt_all string [ "." ] & info [ "L"; "loadpath" ] ~doc)
    in
    let memlimit =
      let doc = "Memory limit (in megabytes)." in
      Arg.(value & opt (some int) None & info [ "m"; "memlimit" ] ~doc)
    in
    let timeout =
      let doc = "Timeout (in seconds)." in
      Arg.(value & opt (some int) None & info [ "t"; "timeout" ] ~doc)
    in
    let prover =
      let all_provers = Prover.list_available () in
      let doc =
        Fmt.str
          "Prover to use. Support is provided for the following provers: %s."
          (Fmt.str "%a"
             (Fmt.list ~sep:Fmt.comma Fmt.string)
             (List.map ~f:Prover.to_string all_provers))
      in
      let provers =
        Arg.enum (List.map ~f:(fun p -> (Prover.to_string p, p)) all_provers)
      in
      Arg.(required & opt (some provers) None & info [ "p"; "prover" ] ~doc)
    in
    let dataset_csv =
      let doc =
        "Dataset under CSV format. Currently only supported by SAVer."
      in
      Arg.(value & opt (some file) None & info [ "dataset-csv" ] ~doc)
    in
    Term.(
      ret
        (const
           (fun format loadpath memlimit timeout prover dataset_csv files _ ->
           `Ok
             (exec_cmd cmdname (fun () ->
                verify format loadpath memlimit timeout prover dataset_csv files)))
        $ format $ loadpath $ memlimit $ timeout $ prover $ dataset_csv $ files
        $ setup_logs))
  in
  Cmd.v info term

let default_info =
  let doc =
    "A platform for characterizing the safety and robustness of artificial \
     intelligence based software."
  in
  let sdocs = Manpage.s_common_options in
  let man =
    [
      `S Manpage.s_common_options;
      `P "These options are common to all commands.";
      `S "MORE HELP";
      `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";
      `S Manpage.s_bugs;
      `P "Email bug reports to <...>";
    ]
  in
  let version = "0.1" in
  let exits = Cmd.Exit.defaults in
  Cmd.info caisar ~version ~doc ~sdocs ~exits ~man

let default_cmd = Term.(ret (const (fun _ -> `Help (`Pager, None)) $ const ()))

let () =
  try
    Cmd.group ~default:default_cmd default_info [ config_cmd; verify_cmd ]
    |> Cmd.eval ~catch:false |> Caml.exit
  with exn when not (log_level_is_debug ()) ->
    Fmt.epr "%a@." Why3.Exn_printer.exn_printer exn
