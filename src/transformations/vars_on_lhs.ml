(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

let make_rt env =
  let th = Env.read_theory env [ "ieee_float" ] "Float64" in
  let t = Theory.ns_find_ts th.th_export [ "t" ] in
  let le = Theory.ns_find_ls th.th_export [ "le" ] in
  let lt = Theory.ns_find_ls th.th_export [ "lt" ] in
  let ge = Theory.ns_find_ls th.th_export [ "ge" ] in
  let gt = Theory.ns_find_ls th.th_export [ "gt" ] in
  let rec rt t =
    let t = Term.t_map rt t in
    match t.t_node with
    | Tapp
        ( ls,
          [
            ({ t_node = Tconst _; _ } as const);
            ({ t_node = Tapp (_, []); _ } as var);
          ] ) ->
      let tt = [ var; const ] in
      if Term.ls_equal ls le
      then Term.ps_app ge tt
      else if Term.ls_equal ls ge
      then Term.ps_app le tt
      else if Term.ls_equal ls lt
      then Term.ps_app gt tt
      else if Term.ls_equal ls gt
      then Term.ps_app lt tt
      else t
    | _ -> t
  in
  let task =
    List.fold [ le; lt; ge; gt ] ~init:(Task.add_ty_decl None t)
      ~f:Task.add_param_decl
  in
  (rt, task)

let vars_on_lhs env =
  let rt, task = make_rt env in
  Trans.rewrite rt task

let init () =
  Trans.register_env_transform
    ~desc:
      "Transformation for provers that need variables on the left-hand-side of \
       logic symbols."
    "vars_on_lhs" vars_on_lhs
