(**************************************************************************)
(*                                                                        *)
(*  This file is part of CAISAR.                                          *)
(*                                                                        *)
(*  Copyright (C) 2022                                                    *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  You can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

open Why3
open Base

let meta_input =
  Theory.(
    register_meta "caisar_input"
      ~desc:"Indicates the input position in the neural network"
      [ MTlsymbol; MTint ])

let meta_output =
  Theory.(
    register_meta "caisar_output"
      ~desc:"Indicates the output position in the neural network"
      [ MTlsymbol; MTint ])

let meta_nn_filename =
  Theory.(
    register_meta_excl "caisar_nnet_or_onnx"
      ~desc:"Indicates the filename of the network" [ MTstring ])

(* Retrieve the (input) variables appearing, as arguments, after an 'nnet_apply'
   symbol. *)
let get_input_variables =
  let rec aux acc (term : Term.term) =
    match term.t_node with
    | Term.Tapp (ls, args) -> (
      match Language.lookup_loaded_nets ls with
      | None -> acc
      | Some _ ->
        let add i acc = function
          | { Term.t_node = Tapp (vs, []); _ } -> Term.Mls.add vs i acc
          | arg ->
            invalid_arg
              (Fmt.str "No direct variable in application: %a" Pretty.print_term
                 arg)
        in
        List.foldi ~init:acc ~f:add args)
    | _ -> Term.t_fold aux acc term
  in
  Trans.fold_decl (fun decl acc -> Decl.decl_fold aux acc decl) Term.Mls.empty

(* Create logic symbols for output variables and simplify the formula. *)
(* TODO: [Reduction_engine] is probably an overkill and should be replaced. *)
let simplify_goal env input_variables =
  let rec aux meta hls (term : Term.term) =
    match term.t_node with
    | Term.Tapp (ls, _) -> (
      match Language.lookup_loaded_nets ls with
      | None -> Term.t_map (aux meta hls) term
      | Some nnet ->
        meta := nnet.filename :: !meta;
        let outputs =
          List.init nnet.nb_outputs ~f:(fun i ->
            let id = Ident.id_fresh "y" in
            let ls = Term.create_fsymbol id [] nnet.ty_data in
            hls := (Decl.create_param_decl ls, ls, i) :: !hls;
            Term.fs_app ls [] nnet.ty_data)
        in
        Term.t_tuple outputs)
    | _ -> Term.t_map (aux meta hls) term
  in
  Trans.fold
    (fun task_hd acc ->
      match task_hd.task_decl.td_node with
      | Use _ | Clone _ | Meta _ -> Task.add_tdecl acc task_hd.task_decl
      | Decl { d_node = Dparam ls; _ } -> (
        let task = Task.add_tdecl acc task_hd.task_decl in
        match Term.Mls.find_opt ls input_variables with
        | None -> task
        | Some pos -> Task.add_meta task meta_input [ MAls ls; MAint pos ])
      | Decl decl ->
        let meta = ref [] in
        let hls = ref [] in
        let decl =
          Decl.decl_map
            (fun term ->
              let term = aux meta hls term in
              if List.is_empty !hls
              then term
              else
                let known =
                  List.fold !hls ~init:task_hd.task_known
                    ~f:(fun acc (d, _, _) -> Decl.known_add_decl acc d)
                in
                let engine =
                  Reduction_engine.create
                    {
                      compute_defs = false;
                      compute_builtin = true;
                      compute_def_set = Term.Sls.empty;
                      compute_max_quantifier_domain = 0;
                    }
                    env known
                in
                Reduction_engine.normalize ~limit:100 engine Term.Mvs.empty term)
            decl
        in
        let acc =
          List.fold !hls ~init:acc ~f:(fun acc (d, ls, i) ->
            let task = Task.add_decl acc d in
            Task.add_meta task meta_output [ MAls ls; MAint i ])
        in
        let acc =
          List.fold !meta ~init:acc ~f:(fun acc s ->
            Task.add_meta acc meta_nn_filename [ MAstr s ])
        in
        Task.add_decl acc decl)
    None

let native_nn_prover env =
  Trans.seq [ Trans.bind get_input_variables (simplify_goal env) ]

let init () =
  Trans.register_env_transform
    ~desc:"Transformation for provers that support loading neural networks."
    "native_nn_prover" native_nn_prover
