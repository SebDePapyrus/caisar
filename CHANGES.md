## 0.1.1 (Unreleased)

- Rework the section in `README.md` for obtaining CAISAR in order to mention the
  `opam` package just released.

## 0.1 (13-07-2022)

First public release of CAISAR.
