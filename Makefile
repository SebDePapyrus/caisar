all:
	dune build --root=. @install caisar.opam caisar-nnet.opam caisar-onnx.opam caisar-ovo.opam

install:
	dune install

uninstall:
	dune uninstall

test:
	dune runtest --root=.

promote:
	dune promote --root=.

docker:
	$(MAKE) -C docker all

clean:
	dune clean


###################################
# opam package creation and release
###################################

PROJECT_ID=1082
TAG=$(shell dune-release tag -f --yes > /dev/null 2>&1 && git describe --tags --abbrev=0)
PKG="caisar-$(TAG).tbz"
PACKAGE_URL="https://git.frama-c.com/api/v4/projects/$(PROJECT_ID)/packages/generic/caisar/$(TAG)/$(PKG)"
DESCRIPTION="$(shell sed -n -e "p;n;:next;/^##/Q;p;n;b next" CHANGES.md | perl -pe 's/\n/\\n/')"

release:
	@echo -n $(DESCRIPTION)
	@echo "Is the CHANGES.md correct for $(TAG) (y/n)?"
	@read yesno; test "$$yesno" = y
	dune-release tag $(TAG)
	dune-release distrib --skip-build --skip-lint
	curl --header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
	--upload-file _build/$(PKG) \
	$(PACKAGE_URL)
	echo $(PACKAGE_URL) > _build/asset-$(TAG).url
	git push origin $(TAG)
	curl --header 'Content-Type: application/json' \
	--header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
	--request POST "https://git.frama-c.com/api/v4/projects/$(PROJECT_ID)/releases" \
	--data '{"name": "Release $(TAG)","tag_name": "$(TAG)", "description": $(DESCRIPTION), "milestones": [], "assets": { "links": [{ "name": "$(PKG)", "url": $(PACKAGE_URL), "link_type":"other" }] } }'
	dune-release opam pkg
	dune-release opam submit

.PHONY: release clean promote test docker uninstall install all
